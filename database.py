#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import aiosqlite
import os

DATABASE_NAME = "/db/highscores.db"


async def initialise() -> None:
    async with aiosqlite.connect(DATABASE_NAME) as db:
        await db.execute(
            """
			CREATE TABLE IF NOT EXISTS scores(
				name text NOT NULL,
				score integer NOT NULL);
            """
        )
        await db.commit()


async def add_score(name: str, score: int) -> None:
    async with aiosqlite.connect(DATABASE_NAME) as db:
        await db.execute("INSERT INTO scores(name, score) VALUES(?, ?)", [name, score])
        await db.commit()


async def fetch_scores() -> dict:
    async with aiosqlite.connect(DATABASE_NAME) as db:
        async with db.execute(
            "SELECT name, score FROM scores ORDER BY score DESC LIMIT 10;"
        ) as cursor:
            rows = await cursor.fetchall()
            response = {
                "scores": [{"name": name, "score": score} for name, score in rows]
            }
            return response
