FROM python:3.7
VOLUME ["/db"]
COPY . /app
WORKDIR /app
RUN pip install -U pip; pip install -Ur requirements.txt
ENTRYPOINT hypercorn -b '0.0.0.0:8000' main:app
