#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import quart
from quart import request
import json

import database

app = quart.Quart(__name__)


@app.route("/api/addscore/", methods=["POST"])
async def handle_new_score():
    data = json.loads((await request.get_data()).decode())
    try:
        name = data["name"]
        score = data["score"]
    except KeyError:
        return "Bad Request", 400
    if isinstance(name, str) and isinstance(score, int):
        await database.add_score(name, score)
        return "OK", 200
    else:
        return "Bad Request", 400


@app.route("/api/highscores/", methods=["GET"])
async def handle_highscores():
    return await database.fetch_scores()


@app.before_serving
async def create_database_if_required():
    await database.initialise()


if __name__ == "__main__":
    app.run()
